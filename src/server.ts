import assert from "assert";
import { Route, Router } from "goodrouter";
import * as http from "http";
import * as http2 from "http2";
import { finished, Readable, Writable } from "stream";
import { Promisable } from "type-fest";
import { MethodNotAvailableError, NoRouteFoundError } from "./errors/index.js";
import { Parameters, Status, Verb } from "./utils/index.js";

let currentHttpIncomingMessage:
    (http.IncomingMessage | http2.Http2ServerRequest) & Readable | undefined;
let currentHttpServerResponse:
    (http.ServerResponse | http2.Http2ServerResponse) & Writable | undefined;
let currentServerIncomingRequest: ServerIncomingRequest | undefined;
let currentAbortSignal: AbortSignal | undefined;

export function getHttpIncomingMessage() {
    assert(currentHttpIncomingMessage != null, "currentHttpIncomingMessage");
    return currentHttpIncomingMessage;
}

export function getHttpServerResponse() {
    assert(currentHttpServerResponse != null, "currentHttpServerResponse");
    return currentHttpServerResponse;
}

export function getServerIncomingRequest() {
    assert(currentServerIncomingRequest != null, "currentServerIncomingRequest");
    return currentServerIncomingRequest;
}

export function getAbortSignal() {
    assert(currentAbortSignal != null, "currentAbortSignal");
    return currentAbortSignal;
}

export interface ServerIncomingRequest {
    path: string;
    query: string;
    method: Verb;
    headers: Parameters;
    stream(signal?: AbortSignal): AsyncIterable<Uint8Array>;
}

export interface ServerOutgoingResponse {
    status: Status;
    headers: Parameters;
    stream?(signal?: AbortSignal): AsyncIterable<Uint8Array>;
}

export interface ServerContext {
    request: ServerIncomingRequest;
    response: ServerOutgoingResponse;
}

export interface ServerMiddleware {
    (
        this: ServerBase,
        route: Route | null,
        request: ServerIncomingRequest,
        next: (request: ServerIncomingRequest) => Promisable<ServerOutgoingResponse>,
    ): Promisable<ServerOutgoingResponse>
}

export interface ServerRouteHandler {
    (
        this: ServerBase,
        route: Route,
        request: ServerIncomingRequest,
    ): Promise<ServerOutgoingResponse>
}

export interface RequestListenerOptions {
    onError?: (error: unknown) => void
}

export interface ServerOperationMetadata {
    requestHeaders: string[];
    responseHeaders: string[];
}

export interface ServerOptions {
    validateIncomingParameters?: boolean;
    validateOutgoingParameters?: boolean;
    validateIncomingEntities?: boolean;
    validateOutgoingEntities?: boolean;
}

export class ServerBase {
    protected validateIncomingParameters: boolean;
    protected validateOutgoingParameters: boolean;
    protected validateIncomingEntities: boolean;
    protected validateOutgoingEntities: boolean;

    protected operations: Record<string, Partial<Record<Verb, ServerRouteHandler>>> = {};

    protected router = new Router({
        decode: value => value,
        encode: value => value,
    });

    constructor(
        options: ServerOptions,
    ) {
        this.validateIncomingParameters = options.validateIncomingParameters ?? true;
        this.validateOutgoingParameters = options.validateOutgoingParameters ?? false;
        this.validateIncomingEntities = options.validateIncomingEntities ?? true;
        this.validateOutgoingEntities = options.validateOutgoingEntities ?? false;
    }

    public registerMiddleware(middleware: ServerMiddleware) {
        const nextMiddleware = this.middleware;

        this.middleware = (route, request, next) => {
            const httpIncomingMessage = getHttpIncomingMessage();
            const httpServerResponse = getHttpServerResponse();
            const abortSignal = getAbortSignal();

            return middleware.call(
                this,
                route,
                request,
                request => {
                    currentHttpIncomingMessage = httpIncomingMessage;
                    currentHttpServerResponse = httpServerResponse;
                    currentServerIncomingRequest = request;
                    currentAbortSignal = abortSignal;

                    return nextMiddleware.call(this, route, request, next);
                },
            );
        };
    }

    public asRequestListener(
        options: RequestListenerOptions = {},
    ): (
        request: (http.IncomingMessage | http2.Http2ServerRequest) & Readable,
        response: (http.ServerResponse | http2.Http2ServerResponse) & Writable,
    ) => void {
        return (
            request,
            response,
        ) => {
            const abortController = new AbortController();
            const { signal } = abortController;

            /*
            Not sure if the aborted event is fired when there is a connection
            error! if we use finish we can be sure that as soon the response
            finished the abort is signalled
     
            One version that uses the aborted event seemed leaky. This might
            be the cause of the leak!
            */
            // request.addListener("aborted", () => abortController.abort());
            finished(response, error => abortController.abort());
            /*
            */

            const task = async () => {
                assert(request.url);
                assert(request.method);

                const urlMatch = /^(.*?)(\?.*)?$/g.exec(request.url);
                assert(urlMatch);

                const incomingRequest = {
                    path: urlMatch[1] ?? "",
                    query: urlMatch[2] ?? "",
                    method: request.method as Verb,
                    headers: request.headers as Parameters,
                    stream(signal?: AbortSignal) {
                        if (signal) {
                            /*
                            aborting the request will drain the request stream
                            */
                            const onAbort = () => request.resume();

                            signal.addEventListener("abort", onAbort);
                            request.addListener("end", () => {
                                signal.removeEventListener("abort", onAbort);
                            });
                        }

                        return request;
                    },
                };

                currentHttpIncomingMessage = request;
                currentHttpServerResponse = response;
                currentServerIncomingRequest = undefined;
                currentAbortSignal = signal;

                const outgoingResponse = await this.receive(incomingRequest);

                currentHttpIncomingMessage = undefined;
                currentHttpServerResponse = undefined;
                currentServerIncomingRequest = undefined;
                currentAbortSignal = undefined;

                response.statusCode = outgoingResponse.status;
                for (
                    const [headerName, headerValue] of
                    Object.entries(outgoingResponse.headers)
                ) {
                    response.setHeader(headerName, headerValue);
                }
                if ("flushHeaders" in response) {
                    response.flushHeaders();
                }

                for await (
                    const chunk of
                    outgoingResponse.stream?.(abortController.signal) ?? []
                ) {
                    await new Promise<void>(
                        (resolve, reject) => response.write(
                            chunk,
                            error => error ? reject(error) : resolve(),
                        ),
                    );
                }
                await new Promise<void>(
                    resolve => response.end(() => resolve()),
                );
            };

            task().catch(async error => {
                options.onError?.(error);
                response.destroy(error);
            });

        };
    }

    protected registerOperation(
        path: string,
        verb: Verb,
        handler: ServerRouteHandler,
    ) {
        this.operations[path] ??= {};
        this.operations[path][verb] = handler;
    }

    private async receive(
        request: ServerIncomingRequest,
    ): Promise<ServerOutgoingResponse> {
        const { path } = request;
        const route = this.router.parseRoute(path);

        const next = async (
            request: ServerIncomingRequest,
        ): Promise<ServerOutgoingResponse> => {

            if (route == null) {
                throw new NoRouteFoundError(path);
            }

            const handlers = this.operations[route.name];
            if (handlers == null) {
                throw new NoRouteFoundError(path);
            }

            const handler = handlers[request.method];
            if (handler == null) {
                throw new MethodNotAvailableError(path, request.method);
            }

            const response = await handler.call(
                this,
                route,
                request,
            );

            return response;
        };

        const httpIncomingMessage = getHttpIncomingMessage();
        const httpServerResponse = getHttpServerResponse();
        const abortSignal = getAbortSignal();

        const response = await this.middleware.call(
            this,
            route,
            request,
            request => {
                currentHttpIncomingMessage = httpIncomingMessage;
                currentHttpServerResponse = httpServerResponse;
                currentServerIncomingRequest = request;
                currentAbortSignal = abortSignal;

                return next(request);
            },
        );

        return response;
    }

    private middleware: ServerMiddleware = (route, request, next) => next(request);
}
