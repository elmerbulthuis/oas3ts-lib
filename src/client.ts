import { HttpSendReceive, httpSendReceive } from "@oas3/http-send-receive";
import { Router } from "goodrouter";
import { Promisable } from "type-fest";
import { Parameters } from "./utils/parameters.js";
import { Status } from "./utils/status.js";
import { Verb } from "./utils/verb.js";

export interface ClientOptions {
    baseUrl: URL;
    validateIncomingParameters?: boolean;
    validateOutgoingParameters?: boolean;
    validateIncomingEntities?: boolean;
    validateOutgoingEntities?: boolean;

    httpSendReceive?: HttpSendReceive;
}

export interface ClientOutgoingRequest {
    url: URL;
    method: Verb;
    headers: Parameters;
    stream?(signal?: AbortSignal): AsyncIterable<Uint8Array>;
}

export interface ClientIncomingResponse {
    status: Status;
    headers: Parameters;
    stream(signal?: AbortSignal): AsyncIterable<Uint8Array>;
}

export interface ClientMiddleware {
    (
        this: ClientBase,
        request: ClientOutgoingRequest,
        next: (request: ClientOutgoingRequest) => Promisable<ClientIncomingResponse>,
    ): Promisable<ClientIncomingResponse>
}

export abstract class ClientBase {
    protected baseUrl: URL;
    protected validateIncomingParameters: boolean;
    protected validateOutgoingParameters: boolean;
    protected validateIncomingEntities: boolean;
    protected validateOutgoingEntities: boolean;

    protected httpSendReceive: HttpSendReceive;

    protected router = new Router({
        decode: value => value,
        encode: value => value,
    });

    constructor(
        options: ClientOptions,
    ) {
        if (options.baseUrl.pathname && !/\/$/.test(options.baseUrl.pathname)) {
            throw new TypeError("pathname should be empty or end with /");
        }
        this.baseUrl = options.baseUrl;

        this.validateIncomingParameters = options.validateIncomingParameters ?? true;
        this.validateOutgoingParameters = options.validateOutgoingParameters ?? false;
        this.validateIncomingEntities = options.validateIncomingEntities ?? true;
        this.validateOutgoingEntities = options.validateOutgoingEntities ?? false;

        if (options.httpSendReceive) {
            this.httpSendReceive = options.httpSendReceive;
        }
        else {
            this.httpSendReceive = httpSendReceive;
        }
    }

    public registerMiddleware(middleware: ClientMiddleware) {
        const nextMiddleware = this.middleware;

        this.middleware = (request, next) => middleware.call(
            this,
            request,
            request => nextMiddleware.call(this, request, next),
        );
    }

    protected async send(
        outgoingRequest: ClientOutgoingRequest,
    ): Promise<ClientIncomingResponse> {
        const { httpSendReceive } = this;

        const next = async () => {
            const {
                headers,
                status,
                body: stream,
            } = await httpSendReceive(
                {
                    url: outgoingRequest.url,
                    headers: outgoingRequest.headers,
                    method: outgoingRequest.method,
                    body: outgoingRequest.stream,
                },
            );

            const incomingResponse: ClientIncomingResponse = {
                status: status as Status,
                headers,
                stream,
            };
            return incomingResponse;
        };

        return this.middleware(outgoingRequest, next);
    }

    private middleware: ClientMiddleware = (request, next) => next(request);

}

