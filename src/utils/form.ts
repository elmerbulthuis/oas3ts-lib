export function formEncode<T extends object>(decoded: T): string {
    const entries = Object.entries(decoded);

    for (const [key, value] of entries) {
        if (typeof value !== "string") {
            throw new Error("only string values supported for form encoding");
        }
    }

    const encoded = entries.
        map(entry => entry.map(encodeURIComponent)).
        map(([key, value]) => key + "=" + value).
        join("&");

    return encoded;
}

export function formDecode<T extends object>(encoded: string): T {
    const entries = encoded.split("&").
        map(part => part.split("=", 2)).
        map(keyValue => keyValue.map(decodeURIComponent));

    const decoded = Object.fromEntries(entries);

    return decoded;
}
