import test from "tape-promise/tape.js";
import { parseAcceptHeader } from "./headers.js";

test("accept", async t => {
    {
        const actual = parseAcceptHeader(
            [
                "text/html; q=0.1, application/octet-stream",
                "text/plain",
            ],
            new Set([
                "application/octet-stream",
                "text/plain",
                "text/html",
            ]),
        );
        const expected = [
            "application/octet-stream",
            "text/plain",
            "text/html",
        ];
        t.deepEqual(actual, expected);
    }
});
