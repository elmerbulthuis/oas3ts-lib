export function stringifyDeepObjectPropertyName(
    objectName: string,
    propertyName: string,
) {
    const parameterName = `${objectName}[${propertyName}]`;
    return parameterName;
}

export function parseDeepObjectPropertyName(
    parameterName: string,
    objectName: string,
) {
    if (!parameterName.startsWith(objectName + "[")) return;
    if (!parameterName.endsWith("]")) return;

    const propertyName = parameterName.substring(
        objectName.length + 1,
        parameterName.length - 1,
    );
    return propertyName;
}
