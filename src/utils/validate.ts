/**
 * 
 * @see https://json-schema.org/draft/2020-12/json-schema-validation.html#rfc.section.6.1.1
 */
export function validateType(
    model: unknown,
    argument: "array" | "object" | "object" | "string" | "number" | "integer" | "boolean",
) {
    switch (argument) {
        case "array":
            return Array.isArray(model);

        case "object":
            return model !== null && typeof model === "object";

        case "string":
            return typeof model === "string";

        case "number":
            return typeof model === "number" && !isNaN(model);

        case "integer":
            return typeof model === "number" && model % 1 === 0 && !isNaN(model);

        case "boolean":
            return typeof model === "boolean";
    }
}

/**
 * 
 * @see https://json-schema.org/draft/2020-12/json-schema-validation.html#rfc.section.6.1.2
 */
export function validateEnum(
    model: unknown,
    argument: unknown[],
) {
    for (const expectValue of argument) {
        if (model === expectValue) return true;
    }
    return false;
}

/**
 * 
 * @see https://json-schema.org/draft/2020-12/json-schema-validation.html#rfc.section.6.1.3
 */
export function validateConst(
    model: unknown,
    argument: unknown,
) {
    return model === argument;
}

/**
 * 
 * @see https://json-schema.org/draft/2020-12/json-schema-validation.html#rfc.section.6.2.1
 */
export function validateMultipleOf(
    model: number,
    argument: number,
) {
    return model % argument === 0;
}

/**
 * 
 * @see https://json-schema.org/draft/2020-12/json-schema-validation.html#rfc.section.6.2.2
 */
export function validateMaximum(
    model: number,
    argument: number,
) {
    return model <= argument;
}

/**
 * 
 * @see https://json-schema.org/draft/2020-12/json-schema-validation.html#rfc.section.6.2.3
 */
export function validateExclusiveMaximum(
    model: number,
    argument: number,
) {
    return model < argument;
}

/**
 * 
 * @see https://json-schema.org/draft/2020-12/json-schema-validation.html#rfc.section.6.2.4
 */
export function validateMinimum(
    model: number,
    argument: number,
) {
    return model >= argument;
}

/**
 * 
 * @see https://json-schema.org/draft/2020-12/json-schema-validation.html#rfc.section.6.2.5
 */
export function validateExclusiveMimimun(
    model: number,
    argument: number,
) {
    return model > argument;
}

/**
 * 
 * @see https://json-schema.org/draft/2020-12/json-schema-validation.html#rfc.section.6.3.1
 */
export function validateMaxLength(
    model: string,
    argument: number,
) {
    return model.length <= argument;
}

/**
 * 
 * @see https://json-schema.org/draft/2020-12/json-schema-validation.html#rfc.section.6.3.2
 */
export function validateMinLength(
    model: string,
    argument: number,
) {
    return model.length >= argument;
}

/**
 * 
 * @see https://json-schema.org/draft/2020-12/json-schema-validation.html#rfc.section.6.3.3
 */
export function validatePattern(
    model: string,
    argument: string,
) {
    const regExp = new RegExp(argument, "u");
    return regExp.test(model);
}

/**
 * 
 * @see https://json-schema.org/draft/2020-12/json-schema-validation.html#rfc.section.6.4.1
 */
export function validateMaxItems(
    model: Array<unknown>,
    argument: number,
) {
    return model.length <= argument;
}

/**
 * 
 * @see https://json-schema.org/draft/2020-12/json-schema-validation.html#rfc.section.6.4.2
 */
export function validateMinItems(
    model: Array<unknown>,
    argument: number,
) {
    return model.length >= argument;
}

/**
 * 
 * @see https://json-schema.org/draft/2020-12/json-schema-validation.html#rfc.section.6.4.3
 */
export function validateUniqueItems(
    model: Array<unknown>,
    argument: boolean,
) {
    if (!argument) return true;

    const set = new Set(model);
    return set.size === model.length;
}

/**
 * 
 * @see https://json-schema.org/draft/2020-12/json-schema-validation.html#rfc.section.6.4.4
 */
export function validateMaxContains(
    model: unknown,
    argument: unknown,
) {
    throw new Error("Not implemented");
}

/**
 * 
 * @see https://json-schema.org/draft/2020-12/json-schema-validation.html#rfc.section.6.4.5
 */
export function validateMinContains(
    model: unknown,
    argument: unknown,
) {
    throw new Error("Not implemented");
}

/**
 * 
 * @see https://json-schema.org/draft/2020-12/json-schema-validation.html#rfc.section.6.5.1
 */
export function validateMaxProperties(
    model: object,
    argument: number,
) {
    return Object.keys(model).length <= argument;
}

/**
 * 
 * @see https://json-schema.org/draft/2020-12/json-schema-validation.html#rfc.section.6.5.2
 */
export function validateMinProperties(
    model: object,
    argument: number,
) {
    return Object.keys(model).length >= argument;
}

/**
 * 
 * @see https://json-schema.org/draft/2020-12/json-schema-validation.html#rfc.section.6.5.3
 */
export function validateRequired<T extends object>(
    model: T,
    argument: Array<keyof T>,
) {
    for (const name of argument) {
        if (model[name] == null) return false;
    }
    return true;
}

/**
 * 
 * @see https://json-schema.org/draft/2020-12/json-schema-validation.html#rfc.section.6.5.4
 */
export function validateDependentRequired(
    model: unknown,
    type: string,
) {
    throw new Error("Not implemented");
}
