import test from "tape-promise/tape.js";
import { formDecode, formEncode } from "./form.js";

test("form encode decode", async t => {
    const original = {
        a: "1",
        b: "2",
    };
    const encoded = formEncode(original);
    const decoded = formDecode(encoded);

    t.equal(encoded, "a=1&b=2");
    t.deepEqual(decoded, original);
});
