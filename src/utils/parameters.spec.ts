import test from "tape-promise/tape.js";
import { getParameterValue, parseParameters } from "./parameters.js";

test("read-parameter", async t => {
    const parameters = {
        "string": ["hi"],
        "number": ["10.5"],
        "integer": ["10"],
    };

    {
        const actual = getParameterValue(parameters, "string");
        const expected = "hi";
        t.equal(actual, expected);
    }
    {
        const actual = getParameterValue(parameters, "no");
        const expected = undefined;
        t.equal(actual, expected);
    }
});

test("parse-parameters", async t => {
    t.deepEqual(
        parseParameters("a:1,b:2", "", ",", ":"),
        { a: "1", b: "2" },
    );

    t.deepEqual(
        parseParameters("a,1,b,2", "", ",", ","),
        { a: "1", b: "2" },
    );
});
