import { ErrorBase } from "./base.js";

export abstract class ClientError extends ErrorBase {

}

export class UnexpectedStatusError extends ClientError {
    public readonly name = "UnexpectedStatusError";

    constructor(
        public status: number,
    ) {
        super(
            `Unexpected status ${status}`,
        );
    }
}

export class MissingResponseContentTypeError extends ClientError {
    public readonly name = "MissingResponseContentTypeError";

    constructor(
    ) {
        super(
            "Missing content-type in response",
        );
    }
}

export class UnexpectedResponseContentType extends ClientError {
    public readonly name = "UnexpectedResponseContentType";

    constructor(
        public contentType: string,
    ) {
        super(
            `Unexpected content-type '${contentType}' in response`,
        );
    }
}

export class OutgoingRequestEntitySerializationError extends ClientError {
    public readonly name = "OutgoingRequestEntitySerializationError";

    constructor() {
        super(
            "Outgoing request entity serialization failed",
        );
    }
}

export class IncomingResponseEntityDeserializationError extends ClientError {
    public readonly name = "IncomingResponseEntityDeserializationError";

    constructor() {
        super(
            "Incoming response entity deserialization failed",
        );
    }
}

export class OutgoingRequestParameterInjectionError extends ClientError {
    public readonly name = "OutgoingRequestParameterInjectionError";

    constructor() {
        super(
            "Outgoing request parameter injection failed",
        );
    }
}

export class IncomingResponseParameterExtractionError extends ClientError {
    public readonly name = "IncomingResponseParameterExtractionError";

    constructor() {
        super(
            "Incoming response parameter extraction failed",
        );
    }
}

export class OutgoingRequestEntityValidationError extends ClientError {
    public readonly name = "OutgoingRequestEntityValidationError";

    constructor(
        public readonly paths: Array<string[]>,
    ) {
        super(
            "Outgoing request entity invalid",
        );
    }
}

export class IncomingResponseEntityValidationError extends ClientError {
    public readonly name = "IncomingResponseEntityValidationError";

    constructor(
        public readonly paths: Array<string[]>,
    ) {
        super(
            "Incoming response entity invalid",
        );
    }
}

export class OutgoingRequestParametersValidationError extends ClientError {
    public readonly name = "OutgoingRequestParametersValidationError";

    constructor(
        public readonly paths: Array<string[]>,
    ) {
        super(
            "Outgoing request parameters invalid",
        );
    }
}

export class IncomingResponseParametersValidationError extends ClientError {
    public readonly name = "IncomingResponseParametersValidationError";

    constructor(
        public readonly paths: Array<string[]>,
    ) {
        super(
            "Incoming response parameters invalid",
        );
    }
}
