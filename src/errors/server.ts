import { Verb } from "../utils/index.js";
import { ErrorBase } from "./base.js";

export abstract class ServerError extends ErrorBase {

}

export class AuthorizationFailedError extends ServerError {
    public readonly name = "AuthorizationFailedError";

    constructor(
        public operation: string,
    ) {
        super(
            `Authorization failed for operation ${operation}`,
        );
    }
}

export class MethodNotAvailableError extends ServerError {
    public readonly name = "MethodNotAvailableError";

    constructor(
        public path: string,
        public method: Verb,
    ) {
        super(
            `Method ${method} not available for path ${path}`,
        );
    }
}

export class NoRouteFoundError extends ServerError {
    public readonly name = "NoRouteFoundError";

    constructor(
        public path: string,
    ) {
        super(
            `No route found for path ${path}`,
        );
    }
}

export class OperationNotRegisteredError extends ServerError {
    public readonly name = "OperationNotRegisteredError";

    constructor(
        public operation: string,
    ) {
        super(
            `Operation ${operation} not registered`,
        );
    }
}

export class MissingRequestContentTypeError extends ServerError {
    public readonly name = "MissingRequestContentTypeError";

    constructor(
    ) {
        super(
            "Missing content-type in request",
        );
    }
}

export class UnexpectedRequestContentType extends ServerError {
    public readonly name = "UnexpectedRequestContentType";

    constructor(
        public contentType: string,
    ) {
        super(
            `Unexpected content-type '${contentType}' in request`,
        );
    }
}

export class IncomingRequestEntityDeserializationError extends ServerError {
    public readonly name = "IncomingRequestEntityDeserializationError";

    constructor() {
        super(
            "Incoming request entity deserialization failed",
        );
    }
}

export class OutgoingResponseEntitySerializationError extends ServerError {
    public readonly name = "OutgoingResponseEntitySerializationError";

    constructor() {
        super(
            "Outgoing response entity serialization failed",
        );
    }
}

export class IncomingRequestParameterExtractionError extends ServerError {
    public readonly name = "IncomingRequestParameterExtractionError";

    constructor() {
        super(
            "Incoming request parameter extraction failed",
        );
    }
}

export class OutgoingResponseParameterInjectionError extends ServerError {
    public readonly name = "OutgoingResponseParameterInjectionError";

    constructor() {
        super(
            "Outgoing response parameter injection failed",
        );
    }
}

export class IncomingRequestEntityValidationError extends ServerError {
    public readonly name = "IncomingRequestEntityValidationError";

    constructor(
        public readonly paths: Array<string[]>,
    ) {
        super(
            "Incoming request entity invalid",
        );
    }
}

export class OutgoingResponseEntityValidationError extends ServerError {
    public readonly name = "OutgoingResponseEntityValidationError";

    constructor(
        public readonly paths: Array<string[]>,
    ) {
        super(
            "Outgoing response entity invalid",
        );
    }
}

export class IncomingRequestParametersValidationError extends ServerError {
    public readonly name = "IncomingRequestParametersValidationError";

    constructor(
        public readonly paths: Array<string[]>,
    ) {
        super(
            "Incoming request parameters invalid",
        );
    }
}

export class OutgoingResponseParametersValidationError extends ServerError {
    public readonly name = "OutgoingResponseParametersValidationError";

    constructor(
        public readonly paths: Array<string[]>,
    ) {
        super(
            "Outgoing response parameters invalid",
        );
    }
}
