export abstract class ErrorBase extends Error {

}

export class SerializationError extends ErrorBase {
    readonly name = "SerializationError";

    constructor() {
        super("serialization error");
    }
}

export class DeserializationError extends ErrorBase {
    readonly name = "DeserializationError";

    constructor() {
        super("deserialization error");
    }
}
