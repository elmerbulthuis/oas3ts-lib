import * as http from "http";
import { ServerBase } from "../server.js";

export interface ServerContextOptions<Server extends ServerBase> {
    serverFactory: () => Server
}
export interface ServerContext<Server extends ServerBase> {
    baseUrl: URL,
    server: Server
}
export async function withServerContext<Server extends ServerBase, T = void>(
    options: ServerContextOptions<Server>,
    job: (context: ServerContext<Server>) => Promise<T>,
) {
    const baseUrl = new URL("http://localhost:8080/");

    const server = options.serverFactory();
    const httpServer = http.createServer(server.asRequestListener({}));
    try {
        await new Promise<void>(
            resolve => httpServer.listen(Number(baseUrl.port), () => resolve()),
        );

        const result = await job({
            server,
            baseUrl,
        });

        return result;
    }
    finally {
        await new Promise<void>(
            (resolve, reject) => httpServer.close(error => error ? reject(error) : resolve()),
        );
    }
}
