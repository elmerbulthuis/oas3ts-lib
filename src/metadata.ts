import { Verb } from "./utils/index.js";

export interface Metadata {
    operationsMap: Record<string, Partial<Record<Verb, string>>>
    availableRequestTypes: string[]
    availableResponseTypes: string[]
    requestHeadersMap: Record<string, string[]>
    responseHeadersMap: Record<string, string[]>
}
