import { Promisable } from "type-fest";
import { DeserializationError, SerializationError } from "../errors/index.js";
import { Status } from "../utils/status.js";

//#region interfaces

export type OutgoingTextRequestDefault<
    P extends object,
    > = {
        readonly parameters: P;
    } & OutgoingTextContainer

export type OutgoingTextRequest<
    P extends object,
    C extends string,
    > = {
        readonly parameters: P;
        readonly contentType: C;
    } & OutgoingTextContainer

export type OutgoingTextResponseDefault<
    S extends Status,
    P extends object,
    > = {
        readonly status: S,
        readonly parameters: P,
    } & OutgoingTextContainer

export type OutgoingTextResponse<
    S extends Status,
    P extends object,
    C extends string,
    > = {
        readonly status: S,
        readonly parameters: P,
        readonly contentType: C;
    } & OutgoingTextContainer

export type IncomingTextRequest<
    P extends object,
    C extends string,
    > = {
        readonly parameters: P;
        readonly contentType: C;
    } & IncomingTextContainer

export type IncomingTextResponse<
    S extends Status,
    P extends object,
    C extends string,
    > = {
        readonly status: S;
        readonly parameters: P;
        readonly contentType: C;
    } & IncomingTextContainer

//#endregion

//#region containers

export type OutgoingTextContainer =
    { stream(signal?: AbortSignal): AsyncIterable<Uint8Array> } |
    { value(): Promisable<string> } |
    { lines(signal?: AbortSignal): AsyncIterable<string> }

export type IncomingTextContainer =
    { stream(signal?: AbortSignal): AsyncIterable<Uint8Array> } &
    { value(): Promisable<string> } &
    { lines(signal?: AbortSignal): AsyncIterable<string> }

//#endregion

//#region serialization

export async function* serializeTextValue(
    valuePromise: Promisable<string>,
): AsyncIterable<Uint8Array> {
    const encoder = new TextEncoder();

    const value = await valuePromise;
    try {
        yield encoder.encode(value);
    }
    catch {
        throw new SerializationError();
    }
}

export async function* serializeTextLines(
    lines: AsyncIterable<string>,
): AsyncIterable<Uint8Array> {
    const encoder = new TextEncoder();

    for await (const line of lines) {
        try {
            yield encoder.encode(line);
            yield encoder.encode("\n");
        }
        catch {
            throw new SerializationError();
        }

    }
}

export async function deserializeTextValue(
    stream: (signal?: AbortSignal) => AsyncIterable<Uint8Array>,
): Promise<string> {
    const decoder = new TextDecoder();

    try {
        let buffer = "";

        for await (const chunk of stream()) {
            buffer += decoder.decode(chunk, { stream: true });
        }

        return buffer;
    }
    catch (error) {
        throw new DeserializationError();
    }

}

export async function* deserializeTextLines(
    stream: (signal?: AbortSignal) => AsyncIterable<Uint8Array>,
    signal?: AbortSignal,
): AsyncIterable<string> {
    const decoder = new TextDecoder();
    const separator = /\r?\n/;
    let buffer = "";

    for await (const chunk of stream(signal)) {
        try {
            buffer += decoder.decode(chunk, { stream: true });

            yield* flush();
        }
        catch {
            throw new DeserializationError();
        }
    }

    if (buffer.length > 0) yield buffer;

    function* flush() {
        while (true) {
            const match = separator.exec(buffer);
            if (!match) break;

            const line = buffer.substring(0, match.index);
            buffer = buffer.substring(match.index + match[0].length);

            yield line;
        }
    }
}

//#endregion
