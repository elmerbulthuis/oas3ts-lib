import { Promisable } from "type-fest";
import { DeserializationError, SerializationError } from "../errors/index.js";
import { Status } from "../utils/index.js";
import { deserializeTextLines, deserializeTextValue } from "./text.js";

//#region interfaces

export type OutgoingJsonRequestDefault<
    P extends object,
    T,
    > = {
        readonly parameters: P;
    } & OutgoingJsonContainer<T>

export type OutgoingJsonRequest<
    P extends object,
    C extends string,
    T,
    > = {
        readonly parameters: P;
        readonly contentType: C;
    } & OutgoingJsonContainer<T>

export type OutgoingJsonResponseDefault<
    S extends Status,
    P extends object,
    T,
    > = {
        readonly status: S,
        readonly parameters: P,
    } & OutgoingJsonContainer<T>

export type OutgoingJsonResponse<
    S extends Status,
    P extends object,
    C extends string,
    T,
    > = {
        readonly status: S,
        readonly parameters: P,
        readonly contentType: C;
    } & OutgoingJsonContainer<T>

export type IncomingJsonRequest<
    P extends object,
    C extends string,
    T,
    > = {
        readonly parameters: P,
        readonly contentType: C;
    } & IncomingJsonContainer<T>

export type IncomingJsonResponse<
    S extends Status,
    P extends object,
    C extends string,
    T,
    > = {
        readonly status: S;
        readonly parameters: P,
        readonly contentType: C;
    } & IncomingJsonContainer<T>

//#endregion

//#region containers

export type OutgoingJsonContainer<T> =
    { stream(signal?: AbortSignal): AsyncIterable<Uint8Array> } |
    { entity(): Promisable<T> } |
    { entities(signal?: AbortSignal): AsyncIterable<T> }

export type IncomingJsonContainer<T> =
    { stream(signal?: AbortSignal): AsyncIterable<Uint8Array> } &
    { entity(): Promisable<T> } &
    { entities(signal?: AbortSignal): AsyncIterable<T> }

//#endregion

//#region serialization

export async function* serializeJsonEntity<T>(
    asyncEntity: Promisable<T>,
): AsyncIterable<Uint8Array> {
    const encoder = new TextEncoder();

    const entity = await asyncEntity;
    try {
        yield encoder.encode(JSON.stringify(entity));
    }
    catch {
        throw new SerializationError();
    }
}

export async function* serializeJsonEntities<T>(
    entities: AsyncIterable<T>,
): AsyncIterable<Uint8Array> {
    const encoder = new TextEncoder();

    for await (const entity of entities) {
        try {
            yield encoder.encode(JSON.stringify(entity));
            yield encoder.encode("\n");
        }
        catch {
            throw new SerializationError();
        }

    }
}

export async function deserializeJsonEntity<T>(
    stream: (signal?: AbortSignal) => AsyncIterable<Uint8Array>,
): Promise<T> {
    const text = await deserializeTextValue(stream);

    try {
        const trimmed = text.trim();
        const entity = JSON.parse(trimmed);

        return entity;
    }
    catch (error) {
        throw new DeserializationError();
    }

}

export async function* deserializeJsonEntities<T>(
    stream: (signal?: AbortSignal) => AsyncIterable<Uint8Array>,
    signal?: AbortSignal,
): AsyncIterable<T> {
    const lines = deserializeTextLines(stream, signal);

    for await (const line of lines) {
        try {
            const trimmed = line.trim();
            if (!trimmed) continue;
            const entity = JSON.parse(trimmed);

            yield entity;
        }
        catch (error) {
            throw new DeserializationError();
        }
    }
}

//#endregion
