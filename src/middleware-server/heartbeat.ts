import { createBufferedIterable } from "buffered-iterable";
import { second } from "msecs";
import { ServerMiddleware, ServerOutgoingResponse } from "../server.js";

export interface HeartbeatMiddlewareOptions {
    /**
     * heartbeat message to send, defaults to newline (\n)
     */
    payload?: Uint8Array;
    /**
     * time to wait to start sending heartbeat messages, default 30 seconds
     */
    interval?: number;
}

export function createHeartbeatMiddleware(
    options: HeartbeatMiddlewareOptions = {},
): ServerMiddleware {
    const payload = options.payload ?? new Uint8Array([10]); // \n
    const interval = options.interval ?? 30 * second;

    return async function (route, request, next) {
        const response = await next(request);
        const { status, headers, stream } = response;
        if (stream == null) return response;

        const responseWithHeartbeat: ServerOutgoingResponse = {
            status,
            headers,
            stream(signal?: AbortSignal) {
                const buffered = createBufferedIterable<Uint8Array>();

                write().catch(error => buffered.error(error));
                return buffered;

                async function write() {
                    const iterable = stream!(signal);

                    let heartbeatTimer = scheduleHeartbeat();
                    try {
                        for await (const chunk of iterable) {
                            clearInterval(heartbeatTimer);
                            buffered.push(chunk);
                            heartbeatTimer = scheduleHeartbeat();
                        }
                    }
                    finally {
                        clearInterval(heartbeatTimer);
                    }

                    buffered.done();
                }

                function scheduleHeartbeat() {
                    return setInterval(
                        () => buffered.push(payload),
                        interval,
                    );
                }
            },
        };

        return responseWithHeartbeat;
    };
}
