import yaml from "js-yaml";
import fetch from "node-fetch";
import test from "tape-promise/tape.js";
import { ServerBase } from "../server.js";
import { withServerContext } from "../testing/index.js";
import { createSpecificationMiddleware } from "./specification.js";

const serverFactory = () => new ServerBase({});

test(
    "specification in server",
    t => withServerContext({ serverFactory }, async ({ server, baseUrl }) => {
        const specification = { message: "hi" };
        const middleware = createSpecificationMiddleware({ specification });
        server.registerMiddleware(middleware);

        {
            const result = await fetch(new URL("openapi.json", baseUrl).toString());
            const actual = await result.text();
            const expected = JSON.stringify(specification);
            t.deepEqual(actual, expected);
        }

        {
            const result = await fetch(new URL("openapi.yaml", baseUrl).toString());
            const actual = await result.text();
            const expected = yaml.dump(specification);
            t.deepEqual(actual, expected);
        }

        {
            const result = await fetch(new URL("openapi.yml", baseUrl).toString());
            const actual = await result.text();
            const expected = yaml.dump(specification);
            t.deepEqual(actual, expected);
        }

    }),
);
