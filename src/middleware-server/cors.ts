import { second } from "msecs";
import { Metadata } from "../metadata.js";
import { ServerMiddleware, ServerOutgoingResponse } from "../server.js";
import { getOperationId, getOperationRequestHeaders, getOperationResponseHeaders, getParameterValue, getParameterValues, getRouteVerbs, Verb } from "../utils/index.js";

export interface CorsMiddlewareOptions {
    maxAge: number
    allowOrigin: string
    metadata: Metadata
}

export function createCorsMiddleware(
    options: CorsMiddlewareOptions,
): ServerMiddleware {
    return async function (this, route, request, next) {
        if (route === null) return next(request);

        let allowOrigin = null;
        for (const origin of getParameterValues(request.headers, "origin")) {
            if (options.allowOrigin !== "*" && options.allowOrigin !== origin) continue;

            allowOrigin = origin;
            break;
        }

        if (allowOrigin === null) return next(request);

        if (request.method === "OPTIONS") {
            const requestMethod = getParameterValue(request.headers, "access-control-request-method");
            if (requestMethod == null) return next(request);

            const operationId = getOperationId(options.metadata, route.name, requestMethod as Verb);
            if (!operationId) return next(request);

            const verbs = getRouteVerbs(options.metadata, route.name);
            const requestHeaders = getOperationRequestHeaders(options.metadata, operationId);
            const responserHeaders = getOperationResponseHeaders(options.metadata, operationId);

            const response: ServerOutgoingResponse = {
                status: 204,
                headers: {
                    "access-control-allow-origin": allowOrigin,
                    "access-control-max-age": (options.maxAge / second).toFixed(0),
                    "access-control-allow-methods": verbs.join(", "),
                    "access-control-allow-headers": requestHeaders.join(", "),
                    "access-control-expose-headers": responserHeaders.join(", "),
                },
            };
            return response;
        }
        else {
            const response = await next(request);
            response.headers["access-control-allow-origin"] = allowOrigin;
            return response;
        }

    };
}
